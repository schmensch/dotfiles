{config, pkgs, ...}:

{
  services = {
    xserver = {
      # displayManager = {
      #   sddm = {
      #     enable = true;
      #     settings.Wayland.SessionDir = "${pkgs.plasma5Packages.plasma-workspace}/share/wayland-sessions";
      #   };
      # };
      desktopManager.plasma5.enable = true;
    };

    hardware.bolt.enable = true;
    power-profiles-daemon.enable = true;
  };

  hardware.bluetooth.enable = true;

  networking = {
    networkmanager.enable = true;
  };

  # xdg.portal = {
  #   extraPortals = with pkgs; [
  #     xdg-desktop-portal-gtk
  #     libsForQt5.xdg-desktop-portal-kde
  #   ];
  #   enable = true;
  #   gtkUsePortal = true;
  # };

  programs = {
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };

  environment = {
    systemPackages = with pkgs; [
      breeze-gtk
      digikam
      krita
      ktorrent
      latte-dock
      libsForQt5.ark
      libsForQt5.filelight
      libsForQt5.gwenview
      libsForQt5.kcalc
      libsForQt5.keysmith
      libsForQt5.kmail
      libsForQt5.plasma-browser-integration
      plasma-thunderbolt
      pinentry-qt
    ];
  };
}
