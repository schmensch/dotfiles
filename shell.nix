{ config, pkgs, ... }:

{
  console = {
    keyMap = "de";
  };

  programs.fish = {
    enable = true;
    shellAbbrs = {
      nv = "nvim";
      nr = "sudo nixos-rebuild --cores 0";
    };
    shellAliases = {
      nv = "nvim";
      nr = "sudo nixos-rebuild --cores 0";
    };
    interactiveShellInit = ''
      function pve
        podman run -it --rm --name=debian -v $argv:/volume debian bash
      end
    '';
  };

  users.defaultUserShell = pkgs.fish;
}
