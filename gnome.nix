{ config, pkgs, ... }:

{
  services.xserver = {
    displayManager = {
      gdm = {
        enable = true;
        wayland = true;
      };
    };
    desktopManager.gnome.enable = true;
  };

  environment = {
    systemPackages = with pkgs; [
      # authenticator
      celluloid
      evolution
      gnome.dconf-editor
      gnome.gnome-boxes
      gnome.gnome-tweaks
      gnome.gnome-sound-recorder
      gnomeExtensions.appindicator
      gnomeExtensions.blur-my-shell
      gnomeExtensions.clipboard-indicator
      gnomeExtensions.color-picker
      gnomeExtensions.espresso
      gnomeExtensions.forge
      gnomeExtensions.just-perfection
      gnomeExtensions.runcat
      gnomeExtensions.transparent-top-bar-adjustable-transparency
      gnomeExtensions.transparent-window-moving
      glimpse
      otpclient
      pinentry_gnome
      shotwell
      xarchiver
    ];

    gnome.excludePackages = with pkgs; [
      gnome-connections
      gnome-tour
      gnome.cheese
      gnome.eog
      gnome.epiphany
      gnome.file-roller
      gnome.geary
      gnome.gedit
      gnome.gnome-calendar
      gnome.gnome-contacts
      gnome.gnome-maps
      gnome.gnome-music
      gnome.gnome-photos
      gnome.gnome-weather
      gnome.simple-scan
      gnome.totem
      gnome.yelp
    ];
  };

  qt5 = {
    enable = true;
    style = "adwaita-dark";
  };

  environment.variables = {
    QT_QPA_PLATFORM = "wayland";
  };
}
