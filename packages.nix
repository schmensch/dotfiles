{ config, pkgs, ... }:

{
  nixpkgs.config = {
    allowUnfree = true;
  };

  programs = {
    chromium = {
      enable = true;
      defaultSearchProviderSearchURL = "https://duckduckgo.com/?kh=1&kae=d&kam=osm&kaj=m&q={searchTerms}";
      defaultSearchProviderSuggestURL = "https://duckduckgo.com/?q={searchTerms}";
      extensions = [
        "bkdgflcldnnnapblkhphbgpggdiikppg" # DuckDuckGo Privacy Essentials
        "nngceckbapebfimnlniiiahkandclblb" # Bitwarden
        "pkehgijcmpdhfbdbbnkijodmdjhbjlgp" # Privacy Badger
        "njdfdhgcmkocbgbhcioffdbicglldapd" # LocalCDN
        "ckkdlimhmcjmikdlpkmbgfkaikojcbjk" # Markdown Viewer
        "cankofcoohmbhfpcemhmaaeennfbnmgp" # Netflix 1080p
        "cjpalhdlnbpafiamejdnhcphjbkeiagm" # uBlock Origin
      ];
    };
    steam.enable = true;
    noisetorch.enable = true;
    neovim = {
      enable = true;
      defaultEditor = true;
      viAlias = true;
      vimAlias = true;
      configure = {
        customRC = ''
          set number
          set tabstop=2
          set shiftwidth=2
          set expandtab
          set softtabstop=0
          set autoindent
          set smarttab
        '';
        packages.myPlugins = with pkgs.vimPlugins; {
          start = [
            YouCompleteMe
            jedi-vim
            ale
          ];
        };
      };
    };
  };

  nixpkgs.config.packageOverrides = pkgs: {
    chromium = pkgs.chromium.override {
      commandLineArgs = [
        "--enable-features=UseOzonePlatform"
        "--ozone-platform=wayland"
        "--disable-background-networking"
        "--disable-reading-from-canvas"
      ];
    };

    vscodium = pkgs.vscodium.overrideAttrs (old: {
      installPhase = old.installPhase + ''
        rm $out/bin/codium

        makeWrapper $out/lib/vscode/codium $out/bin/codium \
          --add-flags "--ozone-platform=wayland --enable-features=UseOzonePlatform"
      '';
    });
  };

  environment.systemPackages = with pkgs; [
    aria2
    aspell
    aspellDicts.de
    aspellDicts.en
    appimage-run
    bat
    bitwarden
    chromium
    cryptsetup
    duperemove
    fd
    ffmpeg
    ffmpegthumbnailer
    ffmpegthumbs
    file
    fishPlugins.done
    fishPlugins.forgit
    fishPlugins.fzf-fish
    fishPlugins.pisces
    fishPlugins.pure
    fzf
    git
    gnupg
    htop
    hunspell
    hunspellDicts.de-de
    hunspellDicts.en-us-large
    jetbrains.goland
    jetbrains.idea-ultimate
    jetbrains.pycharm-professional
    libreoffice-fresh
    minecraft
    multimc
    neofetch
    nixpkgs-fmt
    protonvpn-gui
    qt5.qtwayland
    s-tui
    signal-desktop
    vscodium
    yt-dlp
  ];
}
