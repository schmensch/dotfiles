# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [ 
      ./basic-desktop.nix
      ./encrypted-dns.nix
      ./fonts.nix
      ./gnome.nix
      ./hardware-configuration.nix
      ./packages.nix
      # ./plasma5.nix
      ./shell.nix
      ./users.nix
    ];

  nix = {
    package = pkgs.nix_2_4;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
   };

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    efi = {
      efiSysMountPoint = "/boot/efi";
      canTouchEfiVariables = true;
    };
    systemd-boot = {
      enable = true;
      editor = false;
      consoleMode = "2";
    };
  };

  networking.hostName = "nixos"; # Define your hostname.

  # Set your time zone.
  # time.timeZone = "Europe/Berlin";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.wlp2s0.useDHCP = true;

  # Virtualisation / Containers
  virtualisation = {
    podman = {
      enable = true;
      # dockerCompat = true;
      # dockerSocket.enable = true;
    };
    # docker.enable = true;
  };

  environment.systemPackages = with pkgs; [ podman-compose ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?
}

