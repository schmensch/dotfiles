{ config, pkgs, ... }:

{
  users = {
    mutableUsers = false;
    extraUsers.jonathan = {
      isNormalUser = true;
      description = "Jonathan Zeller";
      extraGroups = [ "wheel" ];
      hashedPassword = ""; # Sanitized
    };
  };
}
