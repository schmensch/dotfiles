{ pkgs, config, ... }:

{
  services = {
    xserver = {
      enable = true;
      layout = "de";
    };

    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };
  };

  sound.enable = false;
  hardware.pulseaudio.enable = false;

  i18n = {
    defaultLocale = "en_US.UTF-8";
    supportedLocales = [ "all" ];
  };

  programs = {
    gnupg.agent = {
      enable = true;
    };
  };
}
